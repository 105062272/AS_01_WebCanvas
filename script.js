var buttonState = [false, false, false, false, false],
    mouseDown = false,
    cX = 0, cY = 0,
    pX = 0, pY = 0;
    entering = false;

//tool variables
var brushSize,
    brushOpacity,
    brushColor;

var eraserSize,
    eraserDepth;

var textFont,
    textSize,
    textColor,
    textIsBold,
    textIsItalic;

var loadedImage;

var currShape,
    shapeColor,
    shapePoint1X,
    shapePoint1Y,
    shapePoint2X,
    shapePoint2Y;

var stepStack = [],
    stepPointer = -1;

function init() {
    init_canvas();
    init_property();
    change_cursor("none");
}


function init_property() {
    //BRUSH
    brushSize = 5;
    brushOpacity = 1;
    brushColor = 'rgb(0,0,0)';

    var BS = document.getElementById("brushsize");
    BS.innerHTML = brushSize;
    var sizeSlider = document.getElementById("brushsizeslider");
    sizeSlider.oninput = function () {
        brushSize = this.value;
        BS.innerHTML = brushSize;
    };

    var BO = document.getElementById("brushopacity");
    BO.innerHTML = brushOpacity;
    var opacitySlider = document.getElementById("brushopacityslider");
    opacitySlider.oninput = function () {
        brushOpacity = this.value;
        BO.innerHTML = brushOpacity;
    };

    var CP = document.getElementById("brushcolorpicker");
    CP.setAttribute("color", brushColor);
    CP.onchange = function () {
        brushColor = this.value;
        //CP.setAttribute("color", brushColor);
    };

    //ERASER

    eraserSize = 10;
    eraserDepth = 1;
    
    var ES = document.getElementById("erasersize");
    ES.innerHTML = eraserSize;
    var esizeSlider = document.getElementById("erasersizeslider");
    esizeSlider.oninput = function () {
        eraserSize = this.value;
        ES.innerHTML = eraserSize;
    };

    var ED = document.getElementById("eraserdepth");
    ED.innerHTML = eraserDepth;
    var depthSlider = document.getElementById("eraserdepthslider");
    depthSlider.oninput = function () {
        eraserDepth = this.value;
        ED.innerHTML = eraserDepth;
    };

    //TEXT
    textFont = 'Arial';
    textSize = 16;
    textColor = 'rgb(0,0,0)';
    textIsBold = false;
    textIsItalic = false;

    var textBox = document.getElementById("textbox");

    var TF = document.getElementById("textfont");
    TF.value = textFont;
    TF.onchange = function () {
        textFont = this.value;   
        textBox.style.fontFamily = textFont;
    }

    var TS = document.getElementById("textsize");
    TS.value = textSize;
    TS.onchange = function () {
        textSize = this.value + 'px';
        textBox.style.fontSize = textSize;
    }

    var TC = document.getElementById("textcolorpicker");
    TC.value = textColor;
    TC.onchange = function () {
        textColor = this.value;
        textBox.style.color = textColor;
    }

    var TB = document.getElementById("textbold");
    TB.onchange = function () {
        textIsBold = this.checked;
        textBox.style.fontWeight = (textIsBold === true ? 'bold' : 'normal');
    }

    var TI = document.getElementById("textitalic");
    TI.onchange = function () {
        textIsItalic = this.checked;
        textBox.style.fontStyle = (textIsItalic === true ? 'italic' : 'normal');
    }

    //SHAPE
    currShape = 'Rectangle';
    shapeColor = 'rgb(0,0,0)';
    shapePoint1X = 0, shapePoint1Y = 0;
    shapePoint2X = 0, shapePoint2Y = 0;

    var SS = document.getElementById("shapeselector");
    SS.onchange = function () {
        currShape = this.value;
    };
    var SCP = document.getElementById("shapecolorpicker");
    SCP.onchange = function () {
        shapeColor = this.value;
    }


    //PIC
    loadedImage = null;
    var iss = document.getElementById("imagesourceselector");
    iss.onchange = function (e) {
        load_image(e);
    }

}

function init_canvas() {
    var canvas = document.getElementById("canvas");

    canvas.addEventListener("mousemove", function (e) { mouse_moved(e) });
    canvas.addEventListener("mousedown", function (e) { mouse_down(e) });
    canvas.addEventListener("mouseup", function (e) { mouse_up(e) });
    canvas.onmouseleave = function () {
        mouseDown = false;
    }
}

function brush_clicked() {
    var brush = document.getElementById("brush"); 
    if (buttonState[0] === true) {
        buttonState[0] = false;
        close_property("brush");
        brush.setAttribute("src", "images/brush.png");
        change_cursor("none");
    }
    else {
        for(var i=0;i<5;i++){
            if (buttonState[i] === true) {
                buttonState[i] = false;
                break;
            }
        }
        document.getElementById("eraser").setAttribute("src", "images/eraser.png");
        document.getElementById("text").setAttribute("src", "images/text.png");
        document.getElementById("shape").setAttribute("src", "images/shape.png");
        document.getElementById("pic").setAttribute("src", "images/pic.png");

        buttonState[0] = true;
        brush.setAttribute("src", "images/brush_clicked.png");

        switch_property("brush");
        change_cursor("brush");
    }
}

function eraser_clicked() {
    var eraser = document.getElementById("eraser");
    if (buttonState[1] === true) {
        buttonState[1] = false;
        close_property("eraser");
        eraser.setAttribute("src", "images/eraser.png");
        change_cursor("none");
    }
    else {
        for (var i = 0; i < 5; i++) {
            if (buttonState[i] === true) {
                buttonState[i] = false;
                break;
            }
        }
        document.getElementById("brush").setAttribute("src", "images/brush.png");
        document.getElementById("text").setAttribute("src", "images/text.png");
        document.getElementById("shape").setAttribute("src", "images/shape.png");
        document.getElementById("pic").setAttribute("src", "images/pic.png");

        buttonState[1] = true;
        eraser.setAttribute("src", "images/eraser_clicked.png");
        switch_property("eraser");
        change_cursor("eraser");
    }
}

function text_clicked() {
    var text = document.getElementById("text");
    if (buttonState[2] === true) {
        buttonState[2] = false;
        close_property("text");
        text.setAttribute("src", "images/text.png");
        change_cursor("none");
    }
    else {
        for (var i = 0; i < 5; i++) {
            if (buttonState[i] === true) {
                buttonState[i] = false;
                break;
            }
        }
        document.getElementById("eraser").setAttribute("src", "images/eraser.png");
        document.getElementById("brush").setAttribute("src", "images/brush.png");
        document.getElementById("shape").setAttribute("src", "images/shape.png");
        document.getElementById("pic").setAttribute("src", "images/pic.png");

        buttonState[2] = true;
        text.setAttribute("src", "images/text_clicked.png");
        switch_property("text");
        change_cursor("text");
    }
}

function shape_clicked() {
    var shape = document.getElementById("shape");
    if (buttonState[3] === true) {
        buttonState[3] = false;
        close_property("shape");
        shape.setAttribute("src", "images/shape.png");
        change_cursor("none");
    }
    else {
        for (var i = 0; i < 5; i++) {
            if (buttonState[i] === true) {
                buttonState[i] = false;
                break;
            }
        }
        document.getElementById("eraser").setAttribute("src", "images/eraser.png");
        document.getElementById("text").setAttribute("src", "images/text.png");
        document.getElementById("brush").setAttribute("src", "images/brush.png");
        document.getElementById("pic").setAttribute("src", "images/pic.png");

        buttonState[3] = true;
        shape.setAttribute("src", "images/shape_clicked.png");
        switch_property("shape");
        change_cursor("shape");
    }
}

function pic_clicked() {
    var pic = document.getElementById("pic");
    if (buttonState[4] === true) {
        buttonState[4] = false;
        close_property("pic");
        pic.setAttribute("src", "images/pic.png");
        change_cursor("none");
    }
    else {
        for (var i = 0; i < 5; i++) {
            if (buttonState[i] === true) {
                buttonState[i] = false;
                break;
            }
        }
        document.getElementById("eraser").setAttribute("src", "images/eraser.png");
        document.getElementById("text").setAttribute("src", "images/text.png");
        document.getElementById("shape").setAttribute("src", "images/shape.png");
        document.getElementById("brush").setAttribute("src", "images/brush.png");

        buttonState[4] = true;
        pic.setAttribute("src", "images/pic_clicked.png");
        switch_property("pic");
        change_cursor("pic");
    }
}

function close_property(type) {
    switch (type) {
        case "brush":
            var brushProperty = document.getElementById("brushProperty");
            brushProperty.style.display = "none";
            break;
        case "eraser":
            var brushProperty = document.getElementById("eraserProperty");
            eraserProperty.style.display = "none";
            break;
        case "text":
            var brushProperty = document.getElementById("textProperty");
            textProperty.style.display = "none";
            break;
        case "shape":
            var brushProperty = document.getElementById("shapeProperty");
            shapeProperty.style.display = "none";
            break;
        case "pic":
            var brushProperty = document.getElementById("picProperty");
            picProperty.style.display = "none";
            break;
        case "any":
            var property = document.getElementsByClassName("property");
            for (i = 0; i < property.length; i++) {
                property[i].style.display = "none";
            }  
    }
}

function switch_property(type) {
    switch (type) {
        case "brush":
            close_property("any");
            var brushProperty = document.getElementById("brushProperty");
            brushProperty.style.display = "inline";
            break;
        case "eraser":
            close_property("any");
            var eraserProperty = document.getElementById("eraserProperty");
            eraserProperty.style.display = "inline";
            break;
        case "text":
            close_property("any");
            var textProperty = document.getElementById("textProperty");
            textProperty.style.display = "inline";
            break;
        case "shape":
            close_property("any");
            var shapeProperty = document.getElementById("shapeProperty");
            shapeProperty.style.display = "inline";
            break;
        case "pic":
            close_property("any");
            var picProperty = document.getElementById("picProperty");
            picProperty.style.display = "inline";
            break;
    }
}

function mouse_moved(e) {
    var canvas = document.getElementById("canvas");
    pX = cX;
    pY = cY;
    cX = e.clientX - canvas.offsetLeft;
    cY = e.clientY - canvas.offsetTop;

    if (buttonState[0]) {
        if (mouseDown) {
            var xd = document.getElementById("canvas").getContext('2d');
            xd.beginPath();
            xd.moveTo(pX, pY);
            xd.lineTo(cX, cY);
            xd.stroke();
        }
    }
    else if (buttonState[1]) {
        if (mouseDown) {
            var xd = document.getElementById("canvas").getContext('2d');
            xd.beginPath();
            xd.moveTo(pX, pY);
            xd.lineTo(cX, cY);
            xd.stroke();
        }
    } 
}

function mouse_down(e) {
    var canvas = document.getElementById("canvas");
    mouseDown = true;
    cX = e.clientX - canvas.offsetLeft;
    cY = e.clientY - canvas.offsetTop;
    //BRUSH
    if (buttonState[0]) {
        var xd = document.getElementById("canvas").getContext('2d');
        xd.lineWidth = brushSize;
        xd.lineCap = 'round';
        xd.strokeStyle = brushColor;
        xd.globalAlpha = brushOpacity;

        xd.globalCompositeOperation = 'source-over';
        xd.beginPath();
        xd.moveTo(cX, cY);
    }
    //ERASER
    else if (buttonState[1]) {
        var xd = document.getElementById("canvas").getContext('2d');
        xd.lineWidth = eraserSize;
        xd.lineCap = 'round';
        xd.strokeStyle = 'rgba(255, 255, 255, 255)';
        xd.globalAlpha = eraserDepth;

        xd.globalCompositeOperation = 'destination-out';
        xd.beginPath();
        xd.moveTo(cX, cY);
    }
    //TEXT
    else if (buttonState[2]) {
        var recX = cX, recY = cY;

        if (!entering) {
            entering = true;
            mouseDown = false;
            var input = document.createElement('input');
            input.type = 'text';
            input.id = "textbox";
            input.style.position = "absolute";
            input.style.left = cX + canvas.offsetLeft + 'px';
            input.style.top = cY + canvas.offsetTop + 'px';
            input.style.fontFamily = textFont;
            input.style.fontSize = textSize;
            input.style.color = textColor;
            input.style.fontWeight = (textIsBold === true ? 'bold' : 'normal');
            input.style.fontStyle = (textIsItalic === true ? 'italic' : 'normal');
            input.onkeydown = function (e) {
                if (e.key === 'Enter') {
                    entering = false;
                    drawText(this.value, recX, recY, this.style.fontFamily,
                        this.style.fontSize, this.style.color, this.style.fontWeight,
                        this.style.fontStyle);
                }
                else {
                    return;
                };                  
            };

            document.body.appendChild(input);
            input.focus();
        }
        else {
            entereing = false;
            document.body.removeChild(document.getElementById("textbox"));
        }    
    }
    //SHAPE
    else if(buttonState[3]) {
        shapePoint1X = cX;
        shapePoint1Y = cY;
    }
    //PIC
    else if(buttonState[4]) {
        if (loadedImage.src !== '') {
            var xd = document.getElementById("canvas").getContext('2d');
            xd.globalCompositeOperation = 'source-over';
            xd.globalAlpha = 1;
            var img = new Image();
            img.src = '';
            xd.drawImage(loadedImage, cX, cY, loadedImage.width, loadedImage.height);
            push_step_stack();
        }
    }
}

function mouse_up(e) {
    var canvas = document.getElementById("canvas");
    mouseDown = false;
    cX = e.clientX - canvas.offsetLeft;
    cY = e.clientY - canvas.offsetTop;

    if (buttonState[3]) {
        shapePoint2X = cX;
        shapePoint2Y = cY;

        var xd = document.getElementById("canvas").getContext('2d');
        xd.globalCompositeOperation = "source-over";
        xd.globalAlpha = 1;
        xd.fillStyle = shapeColor;
        switch (currShape) {
            case "Rectangle":
                xd.beginPath();
                xd.moveTo(shapePoint1X, shapePoint1Y);
                xd.lineTo(shapePoint2X, shapePoint1Y);
                xd.lineTo(shapePoint2X, shapePoint2Y);
                xd.lineTo(shapePoint1X, shapePoint2Y);
                xd.fill();
                break;
            case "Circle":
                var dx = Math.abs(shapePoint1X - shapePoint2X),
                    dy = Math.abs(shapePoint1Y - shapePoint2Y);
                var midPointX = shapePoint1X + dx / 2,
                    midPointY = shapePoint1Y + dy / 2;
                var radius = Math.sqrt(dx * dx + dy * dy) / 2;
                xd.beginPath();
                xd.arc(midPointX, midPointY, radius, 0, Math.PI * 2, true);
                xd.fill();
                break;
            case "Triangle":
                xd.beginPath();
                xd.moveTo(shapePoint1X, shapePoint1Y);
                xd.lineTo(shapePoint2X, shapePoint2Y);
                var dx = Math.abs(shapePoint1X - shapePoint2X);
                xd.lineTo(shapePoint2X - 2 * dx, shapePoint2Y);
                xd.fill();
                break;
        }
    }

    if(buttonState[0] || buttonState[1] || buttonState[3]){
        push_step_stack();
    }

}

function drawText(text, x, y, font, size, color, weight, style) {
    var textToDraw = text;
    document.body.removeChild(document.getElementById("textbox"));

    var xd = document.getElementById("canvas").getContext('2d');
    xd.font = weight + " " + style + " " + size + " " + font;
    xd.fillStyle = color;
    xd.textBaseline = 'top';
    xd.globalCompositeOperation = 'source-over';
    xd.globalAlpha = 1;
    xd.fillText(textToDraw, x, y);

    push_step_stack();
}

function clear_canvas() {
    var canvas = document.getElementById("canvas");
    var xd = canvas.getContext('2d');
    xd.clearRect(0, 0, canvas.clientWidth, canvas.clientHeight);
}

function undo_canvas() {
    if (stepPointer !== -1) {
        clear_canvas();
        var xd = document.getElementById("canvas").getContext('2d');
        xd.drawImage(stepStack[--stepPointer], 0, 0);
    }
    else {
        window.alert("plz stop");
    }
}

function redo_canvas() {
    var xd = document.getElementById("canvas").getContext('2d');
    if (stepPointer < (stepStack.length - 1)) {
        xd.drawImage(stepStack[++stepPointer], 0, 0);
    }
    else {
        window.alert("am I a joke to u?");
    }
}

function push_step_stack() {
    var canvasImgURL = document.getElementById("canvas").toDataURL();
    var canvasImg = new Image();
    canvasImg.src = canvasImgURL;
    stepPointer++;

    if (stepPointer > (stepStack.length - 1)) {
        stepStack.push(canvasImg);
    }
    else {
        stepStack[stepPointer] = canvasImg;
    }
}

function load_image(e) {
    var URL = window.URL;
    var url = URL.createObjectURL(e.target.files[0]);

    var img = new Image();
    img.src = url;
    loadedImage = img;
}

function download_image() {
    var canvas = document.getElementById("canvas");
    var img = canvas.toDataURL("image/png");
    return img;
}

function change_cursor(type) {
    var canvas = document.getElementById("canvas");

    switch (type) {
        case "brush":
            canvas.style.cursor = "url(images/brush_cursor.png), auto";
            break;
        case "eraser":
            canvas.style.cursor = "url(images/eraser_cursor.png), auto";
            break;
        case "text":
            canvas.style.cursor = "url(images/text_cursor.png), auto";
            break;
        case "shape":
            canvas.style.cursor = "url(images/shape_cursor.png), auto";
            break;
        case "pic":
            canvas.style.cursor = "url(images/pic_cursor.png), auto";
            break;
        case "none":
            canvas.style.cursor = "url(images/cursor.png), auto";
            break;
        
    }
}
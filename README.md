# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="images/demo.gif" width="1000px" height="500px"></img>

## Completeness

| **Function**                                     | **Done**  |
| :----------------------------------------------: | :-------: |
| Brush                                            | :ok_hand: |
| Eraser                                           | :ok_hand: |
| Color selector                                   | :ok_hand: |
| Simple menu                                      | :ok_hand: |
| User can type texts on canvas                    | :ok_hand: |
| Font menu                                        | :ok_hand: |
| Cursor icon                                      | :ok_hand: |
| Refresh button                                   | :ok_hand: |
| :------------60%-score-above-------------------: | :-------: |
| Different brush shape                            | :ok_hand: |
| Undo/Redo button                                 | :ok_hand: |
| Image tool                                       | :ok_hand: |
| Download                                         | :ok_hand: |
| :------------95%-score-above-------------------: | :-------: |
| eye-protecting (good looking) UI                 |   :joy:   |
| :-----------100%-score-above-------------------: | :-------: |

## Some bugs
* 三角形反方向拉出來會很怪
* Text box 輸入時，如果沒有按 Enter 直接點旁邊會壞掉
* **NUKE 按下去只是清除版面卻不會真的爆炸**

## Please subscribe to PEWDIEPIE !!